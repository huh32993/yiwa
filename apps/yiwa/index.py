from apps import app
from flask import request, jsonify
from utils.db import select

"""伊瓦视图，本应用属于家庭私有使用，不是云端集体部署，功能也是自由切换，就不使用“可插拨视图”功能了。"""


@app.route("/yiwa", methods=["POST"])
def yiwa():
    if request.method == "POST":
        sql = """SELECT status, caption, stt FROM yiwa LIMIT 1"""
        status, caption, stt = select(sql)[0]
        return jsonify(result = locals())

@app.route("/commands")
def commands():
    """全部指令"""
    pass